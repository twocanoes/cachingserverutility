//
//  TCSCacheManager.m
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/26/21.
//

#import "TCSCacheManager.h"
#import "TCTaskWrapperWithBlocks.h"
#import "TCSAssetData.h"
@interface TCSCacheManager()
@property (strong) TCTaskWrapperWithBlocks *tw;
@property (nonatomic, copy) void (^completionBlock)(TCSAssetData *);
@end
@implementation TCSCacheManager
+ (instancetype)shared {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[[self class] alloc] init];


    });
    return sharedMyManager;
}
-(void)settingsWithCompletionBlock:(void (^)(TCSAssetData *))completionBlock{
    self.completionBlock=completionBlock;
    self.cachingServers=[NSMutableArray array];
    self.tw=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
    } endBlock:^{
    } outputBlock:^(NSString *output) {
        TCSAssetData *assetData=[[TCSAssetData alloc] initWithJSON:output];

        [self populateServers:assetData];
        completionBlock(assetData);
    } errorOutputBlock:^(NSString *errorOutput) {
    } arguments:@[@"/usr/bin/AssetCacheLocatorUtil",@"--json"]];

    [self.tw startProcess];
    

}
-(void)populateServers:(TCSAssetData *)assets{

    [assets.clientCacheInfo.refreshedServers.personalCaching enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.currentUserPersonalCaching=YES;


    }];

    [assets.clientCacheInfo.refreshedServers.personalCachingAndImport enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.currentUserPersonalCachingAndImport=YES;

    }];
    [assets.clientCacheInfo.refreshedServers.sharedCaching enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.currentUserSharedCaching=YES;

    }];


    [assets.systemCacheInfo.refreshedServers.personalCaching enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.systemPersonalCaching=YES;

    }];

    [assets.systemCacheInfo.refreshedServers.personalCachingAndImport enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.systemPersonalCachingAndImport=YES;


    }];
    [assets.systemCacheInfo.refreshedServers.sharedCaching enumerateObjectsUsingBlock:^(TCSCacheServerInfo * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        TCSCachingServer *cachingServer=[[TCSCachingServer alloc] init];
        cachingServer.info=obj;
        if ([self.cachingServers containsObject:cachingServer]==NO){
            [self.cachingServers addObject:cachingServer];
        }
        NSInteger i=[self.cachingServers indexOfObject:cachingServer];
        TCSCachingServer *foundServer=[self.cachingServers objectAtIndex:i];
        foundServer.systemSharedCaching=YES;


    }];
    
}
@end
