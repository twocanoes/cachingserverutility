//
//  AppDelegate.h
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/26/21.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

