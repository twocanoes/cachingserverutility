//
//  TCSAssetData.h
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/27/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSAssetData : NSObject
- (instancetype)initWithJSON:(NSString *)json;
@end

@interface TCSCacheVersions : NSObject

@property (strong) NSString* assetCacheLocatorUtil;
@property (strong) NSString* framework;

@end

@interface TCSCacheServerAdvice : NSObject

@property (assign) float connectTimeout;
@property (assign) BOOL supportsUrgency;
@property (strong) NSString *validUntil;

@end

@interface TCSCacheServerInfo : NSObject
@property (strong) TCSCacheServerAdvice *advice;
@property (assign) BOOL favored;
@property (strong) NSString *guid;
@property (assign) BOOL healthy;
@property (strong) NSString *hostport;
@property (assign) float rank;

@end
@interface TCSCacheServerCollect : NSObject
@property (strong) NSArray <TCSCacheServerInfo *> *allServers;
@property (strong) NSArray <TCSCacheServerInfo *> *personalCaching;
@property (strong) NSArray <TCSCacheServerInfo *> *personalCachingAndImport;
@property (strong) NSArray <TCSCacheServerInfo *> *sharedCaching;

@end


@interface TCSCacheInfo : NSObject

@property (assign) BOOL mightHave;
@property (strong) NSArray *refreshFavoredServerRanges;
@property (strong) NSArray *refreshedPublicIPAddressRanges;
@property (strong) TCSCacheServerCollect *refreshedServers;
@property (strong) NSArray <NSString *> *savedFavoredServerRanges;
@property (strong) NSArray <NSString *> *savedPublicIPAddressRanges;
@property (strong) TCSCacheServerCollect *savedServers;
@end

@interface TCSCachingServer :NSObject
@property (strong) TCSCacheServerInfo *info;
@property (assign) BOOL systemPersonalCaching;
@property (assign) BOOL systemPersonalCachingAndImport;
@property (assign) BOOL systemSharedCaching;
@property (assign) BOOL currentUserPersonalCaching;
@property (assign) BOOL currentUserPersonalCachingAndImport;
@property (assign) BOOL currentUserSharedCaching;
@end

@interface TCSAssetData()
@property (strong) NSString *publicIPAddress;
@property (strong) NSArray <NSString *> *reachability;
@property (strong) TCSCacheInfo *clientCacheInfo;
@property (strong) TCSCacheInfo *systemCacheInfo;
@property (strong) TCSCacheVersions *versions;
@end



NS_ASSUME_NONNULL_END
