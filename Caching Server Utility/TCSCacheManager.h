//
//  TCSCacheManager.h
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/26/21.
//

#import <Foundation/Foundation.h>
#import "TCSAssetData.h"
NS_ASSUME_NONNULL_BEGIN

@interface TCSCacheManager : NSObject
+ (instancetype)shared ;
@property (strong) NSMutableArray <TCSCachingServer *> *cachingServers;
-(void)settingsWithCompletionBlock:(void (^)(TCSAssetData *))completionBlock;
@end

NS_ASSUME_NONNULL_END
