//
//  TCSCheckbox.h
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/28/21.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSCheckbox : NSButton

@end

NS_ASSUME_NONNULL_END
