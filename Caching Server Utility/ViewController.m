//
//  ViewController.m
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/26/21.
//

#import "ViewController.h"
#import "TCSCacheManager.h"
@interface ViewController()
@property (strong) TCSCacheManager *cacheManager;
@property (strong) NSArray *cacheServers;
@end
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.cacheManager=[TCSCacheManager shared];
    [self reloadData:self];

}

- (IBAction)refresh:(id)sender {


    [self reloadData:self];

}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


-(void)reloadData:(id)sender{
    self.cacheServers=nil;
    [self.cacheManager settingsWithCompletionBlock:^(TCSAssetData * val) {


        dispatch_async(dispatch_get_main_queue(), ^{
            self.assetData=val;

            self.cacheServers=self.cacheManager.cachingServers;
        });

    }];

}
@end
