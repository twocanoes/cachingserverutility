//
//  ViewController.h
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/26/21.
//

#import <Cocoa/Cocoa.h>
#import "TCSAssetData.h"

@interface ViewController : NSViewController

@property (strong) TCSAssetData *assetData;
@end

