//
//  TCSAssetData.m
//  Caching Server Utility
//
//  Created by Timothy Perfitt on 5/27/21.
//


#import "TCSAssetData.h"

#define ASSETCACHERESULTSKEY @"results"
#define ASSETCACHECURRENTUSERKEY @"current user"
#define ASSETCACHEMIGHTHAVEKEY @"might have"
#define ASSETCACHEREFRESHEDFAVOREDSERVERRANGES @"refreshed favored server ranges"
#define ASSETCACHEREFRESHEDSERVERS @"refreshed servers"
#define ASSETCACHEALLSERVERS @"all servers"
#define ASSETCACHEPERSONALCACHING @"personal caching"
#define ASSETCACHESHAREDCACHING @"shared caching"
#define ASSETCACHEPERSONALCACHINGANDIMPORT @"personal caching and import"

#define ASSETCACHSAVEDFAVOREDSERVERRANGESEKEY @"saved favored server ranges"
#define ASSETCACHESAVEDPUBLICIPADDRESSRANGESKEY @"saved public IP address ranges"
#define ASSETCACHESAVEDSERVERSKEY @"saved servers"
#define ASSETCACHEPUBLICIPADDRESSKEY @"public IP address"
#define ASSETCACHEREACHABILITYKEY @"reachability"
#define ASSETCACHESYSTEMKEY @"system"
#define ASSETCACHEREFRESHEDPUBLICIOPADDRESSRANGESKEY @"refreshed public IP address ranges"
#define ASSETCACHEPERSONALCACHINGANDIMPORTKEY @"personal caching and import"
#define ASSETCACHEVERSIONSKEY @"versions"
#define ASSETCACHELOCATORUTILKEY @"AssetCacheLocatorUtil"
#define ASSETCACHEFRAMEWORKKEY @"framework"
#define ASSETCACHEADVICE @"advice"
#define ASSETCACHECONNECTTIMEOUT @"connectTimeout"
#define ASSETCACHESUPPORTSURGENCY @"supportsUrgency"
#define ASSETCACHEVALIDUNTIL @"validUntil"
#define ASSETCACHEFAVORED @"favored"
#define ASSETCACHEGUID @"guid"
#define ASSETCACHEHEALTHY @"healthy"
#define ASSETCACHEHOSTPORT @"hostport"
#define ASSETCACHERANK @"rank"

@implementation TCSCacheVersions

- (NSString *)description
{

    return [NSString stringWithFormat:@"assetCacheLocatorUtil:%@, framework:%@", self.assetCacheLocatorUtil,self.framework];
}

@end

@implementation TCSCacheServerAdvice
- (instancetype)initWithAdviceInfo:(NSDictionary *)adviceInfo
{
    self = [super init];
    if (self) {
        self.connectTimeout=[adviceInfo[ASSETCACHECONNECTTIMEOUT] floatValue];
        self.supportsUrgency=[adviceInfo[ASSETCACHESUPPORTSURGENCY] boolValue];
        self.validUntil=adviceInfo[ASSETCACHEVALIDUNTIL];
    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"connectTimeout:%2.2f, supportUgency:%@, validUntil:%@", self.connectTimeout,self.supportsUrgency==YES?@"Yes":@"No",self.validUntil];
}
@end

@implementation TCSCacheServerInfo
+(NSArray <TCSCacheServerInfo *> *)serverArrayFromArray:(NSArray *)inArray{
    NSMutableArray <TCSCacheServerInfo * > * serverInfoArray=[NSMutableArray array];

    [inArray enumerateObjectsUsingBlock:^(NSDictionary *currServerInfo, NSUInteger idx, BOOL * _Nonnull stop) {
        [serverInfoArray addObject:[[TCSCacheServerInfo alloc] initWithServerInfo:currServerInfo]];
    }];

    return [NSArray arrayWithArray:serverInfoArray];


}



- (instancetype)initWithServerInfo:(NSDictionary *)serverInfo
{
    self = [super init];
    if (self) {
        if (serverInfo[ASSETCACHEADVICE]) {
            self.advice=[[TCSCacheServerAdvice alloc] initWithAdviceInfo:serverInfo[ASSETCACHEADVICE]];
            self.favored=[serverInfo[ASSETCACHEFAVORED] boolValue];
            self.guid=serverInfo[ASSETCACHEGUID];
            self.healthy=[serverInfo[ASSETCACHEHEALTHY] boolValue];
            self.rank=[serverInfo[ASSETCACHERANK] floatValue];
            self.hostport=serverInfo[ASSETCACHEHOSTPORT];
        }

    }
    return self;
}
- (BOOL)isEqualTo:(TCSCacheServerInfo *)object{

    if ([self.guid.uppercaseString isEqualToString:object.guid.uppercaseString] &&
        self.healthy==object.healthy &&
        self.rank==object.rank &&
        self.advice.connectTimeout==object.advice.connectTimeout &&
        self.advice.supportsUrgency==object.advice.supportsUrgency
        ){
        return YES;
    }
    return NO;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"advice: %@, favored: %@, guid:%@, healthy:%@, rank:%2.2f",
            self.advice,
            self.favored==YES?@"Yes":@"No",
            self.guid,
            self.healthy==YES?@"Yes":@"No",
            self.rank];
}
@end

@implementation TCSCacheServerCollect

- (instancetype)initWithServerCollectionDictionary:(NSDictionary *)inCollection
{
    self = [super init];
    if (self) {
        //inCollection
        self.allServers=[TCSCacheServerInfo serverArrayFromArray:inCollection[ASSETCACHEALLSERVERS]];
        self.personalCaching=[TCSCacheServerInfo serverArrayFromArray:inCollection[ASSETCACHEPERSONALCACHING]];
        self.personalCachingAndImport=[TCSCacheServerInfo serverArrayFromArray:inCollection[ASSETCACHEPERSONALCACHINGANDIMPORT]];
        self.sharedCaching=[TCSCacheServerInfo serverArrayFromArray:inCollection[ASSETCACHESHAREDCACHING]];

    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"allServers:%@, personalCaching:%@,personalCachingAndImport:%@, sharedCaching:%@ ", self.allServers,self.personalCaching,self.personalCachingAndImport,self.sharedCaching];
}
@end
@implementation TCSCacheInfo

- (instancetype)initWithCacheInfo:(NSDictionary *)cacheInfo
{
    self = [super init];
    if (self) {
        self.mightHave=[cacheInfo[ASSETCACHEMIGHTHAVEKEY] boolValue];


        self.refreshedServers=[[TCSCacheServerCollect alloc] initWithServerCollectionDictionary:cacheInfo[ASSETCACHEREFRESHEDSERVERS]];

        self.refreshedPublicIPAddressRanges=cacheInfo[ASSETCACHEREFRESHEDPUBLICIOPADDRESSRANGESKEY];
        self.savedFavoredServerRanges=cacheInfo[ASSETCACHEREFRESHEDFAVOREDSERVERRANGES];
        self.savedPublicIPAddressRanges=cacheInfo[ASSETCACHESAVEDPUBLICIPADDRESSRANGESKEY];

        self.savedServers=[[TCSCacheServerCollect alloc] initWithServerCollectionDictionary:cacheInfo[ASSETCACHESAVEDSERVERSKEY]];

    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"mightHave:%@, refreshFavoredServerRanges:%@, refreshedPublicIPAddressRanges:%@, refreshedServers: %@ savedFavoredServerRanges:%@, savedPublicIPAddressRanges:%@, savedServers:%@", self.mightHave==YES?@"Yes":@"No",self.refreshFavoredServerRanges,self.refreshedPublicIPAddressRanges,self.refreshedServers,
            self.savedFavoredServerRanges,self.savedPublicIPAddressRanges,self.savedServers];
}
@end


@implementation TCSAssetData

- (instancetype)initWithJSON:(NSString *)json
{
    self = [super init];
    if (self) {
        NSData *jsonData=[json dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *assetInfo = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];

        NSDictionary *results=assetInfo[ASSETCACHERESULTSKEY];
        NSDictionary *versions=assetInfo[ASSETCACHEVERSIONSKEY];

        self.publicIPAddress=results[ASSETCACHEPUBLICIPADDRESSKEY];
        self.reachability=results[ASSETCACHEREACHABILITYKEY];

        TCSCacheVersions *cacheVersions=[[TCSCacheVersions alloc] init];
        cacheVersions.assetCacheLocatorUtil=versions[ASSETCACHELOCATORUTILKEY];
        cacheVersions.framework=versions[ASSETCACHEFRAMEWORKKEY];

        self.versions=cacheVersions;

        self.clientCacheInfo=[[TCSCacheInfo alloc] initWithCacheInfo:results[ASSETCACHECURRENTUSERKEY]];

        self.systemCacheInfo=[[TCSCacheInfo alloc] initWithCacheInfo:results[ASSETCACHESYSTEMKEY]];
//        if ( [[self.systemCacheInfo.refreshedServers.allServers objectAtIndex:0] isEqualTo:[self.clientCacheInfo.refreshedServers.allServers objectAtIndex:0]]){
//
//            NSLog(@"good");
//        }
    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"publicIP: %@, reachability: %@, clientCacheInfo:%@, systemCacheInfo:%@, versions:%@",self.publicIPAddress,self.reachability,self.clientCacheInfo,self.systemCacheInfo,self.versions];
}

@end
@implementation TCSCachingServer


- (BOOL)isEqual:(TCSCachingServer *)object{

    return [self.info isEqualTo:object.info];

}
/*
 @property (strong) TCSCacheServerInfo *info;
 @property (assign) BOOL systemPersonalCaching;
 @property (assign) BOOL systemPersonalCachingAndImport;
 @property (assign) BOOL systemSharedCaching;
 @property (assign) BOOL currentUserPersonalCaching;
 @property (assign) BOOL currentUserPersonalCachingAndImport;
 @property (assign) BOOL currentUserSharedCaching;

 */
- (NSString *)description
{
    return [NSString stringWithFormat:@"info:%@, systemPersonalCaching: %@, systemPersonalCachingAndImport:%@, systemSharedCaching:%@, currentUserPersonalCaching: %@, currentUserPersonalCachingAndImport:%@, currentUserSharedCaching:%@",
            self.info.description,
            self.systemPersonalCaching==YES?@"Yes":@"No",
            self.systemPersonalCachingAndImport==YES?@"Yes":@"No",
            self.systemSharedCaching==YES?@"Yes":@"No",
            self.currentUserPersonalCaching==YES?@"Yes":@"No",
            self.currentUserPersonalCachingAndImport==YES?@"Yes":@"No",
            self.currentUserSharedCaching==YES?@"Yes":@"No"
            ];
}
@end
